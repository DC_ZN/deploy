#!/usr/bin/env bash

# include config
THIS=`readlink -f "${BASH_SOURCE[0]}" 2>/dev/null||echo $0`
DIR=`dirname "${THIS}"`
. "$DIR/env.sh"

# initialise the genesis block
geth --datadir=$datadir init ./yocorp.json

# run mining in the background thread
geth --nodekey ../miner_node.key --nodiscover --networkid $networkid --datadir=$datadir --ipcdisable --rpc --unlock $main_acc_addr --password <(echo $main_acc_pass) --mine --rpcaddr 127.0.0.1 --rpcapi eth,net,web3,personal &

# unlock account
sleep 1 && geth --nodekey ../miner_node.key --nodiscover --networkid $networkid --datadir=$datadir attach rpc:http://127.0.0.1:8545 <<EOF
personal.unlockAccount("${fund_holder_acct}", "${fund_holder_pass}", 5000);
EOF

# deploy smart contracts
truffle compile && truffle deploy --network private

# Make sure that the contract is stored on the network
sleep 10
