#!/usr/bin/env bash

THIS=`readlink -f "${BASH_SOURCE[0]}" 2>/dev/null||echo $0`
DIR=`dirname "${THIS}"`
. "$DIR/env.sh"

geth --nodekey ../miner_node.key --nodiscover --networkid $networkid --datadir=$datadir --ipcdisable --unlock $main_acc_addr --password <(echo $main_acc_pass) --mine
