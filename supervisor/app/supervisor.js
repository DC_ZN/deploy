const stream = require('stream');
const pwgen = require('generate-password');
const util = require("util");
const fs = require("fs");
const gpg = require("gpg");

// NOTE: hard-coded name of onionspawn container
const onionspawn_name = 'deploy_onionspawn_1';


// Return onionspawn container info from a list of container info's
// TODO: improve so it doesn't depend on hard-coded name??
function findOnionspawnContainer(containers) {
  for (let container of containers) {
    for (let name of container.Names) {
      if (name.includes(onionspawn_name)) {
        //console.log('Onionspawn container found, id=' + container.Id)
        return container;
      }
    }
  }
}

// Returns 1 if chunk contains count > 0 (with utf8 mark in the beginning).
function checkChunk(chunk) {
  if (chunk.length == 0) {
    console.log("Empty chunk while reading output from for nginx access log command");
    return -1;
  }

  spl = chunk.split(' ');
  count = spl[0];
  if (count.length > 8) {// ignore initial utf8 mark
    count_good = count.substring(8);
    count_int = parseInt(count_good, 10);
    if (count_int != NaN && count_int > 0) {
      return 1;
    }
  }
  return -1;
}

const Express = require('express');
const app = new Express();

const BodyParser = require('body-parser');
app.use(BodyParser.json());

const Docker = require('dockerode');
const docker = new Docker();


const port = 3000;
var router2 = Express.Router();

// ====== V2 ========

// Suffices for user containers and networks
const _web = '_web'; // onion_web
const _ssh = '_ssh'; // onion_ssh
const _boss = '_boss'; // mitm_quest
const _pnet = '_pnet'; // personal network of onion_ssh and mitm_quest

router2.post('/', async (req, res) => {
  let snb = "undefined_snb";
  try {
    snb = req.body.service_name; // FIXME: validate! must be unix name for a directory...
    console.log(snb, 'Service name base: ' + snb);
    
    const boss_password = pwgen.generate({length: 10, numbers: true});
    console.log(snb, 'Generated boss password: ' + boss_password);
    
    await encrypt_key("key.txt", "/tmp/" + snb + boss_password, boss_password);
    encrypted_key = fs.readFileSync("/tmp/" + snb + boss_password, "utf8");//.replace(/\n/g,"|");
    console.log(snb, 'Encrypted key: ' + encrypted_key);
    fs.unlink("/tmp/" + snb + boss_password, (err) => {
      if(err) console.log(snb, err);
      console.log(snb, "encrypted keyfile deleted");
    });
    
    let onionweb_env = ['HIDDENSERVICE_NAME=' + snb + _web];
    let onionssh_env = ['HIDDENSERVICE_NAME=' + snb + _ssh,
                        'HIDDENSERVICE_PORT=22',
                        'GOODKEY=' + encrypted_key ];
    
    let mitmquest_env = ['BOSS_PASSWORD=' + boss_password];

    let personal_tor_network_name = snb + _pnet;
    
    const personal_tor_network = await docker.createNetwork({ Name: personal_tor_network_name });
    await personal_tor_network.connect({ Container: onionspawn_name });
      
    // TODO: make the network fully internal but operational? need to add yo-corp.ru (nginx_proxy),
    // simple DNS server and that's it?

    const onionweb_container = await docker.createContainer({
      Image: 'onion_web',
      name: snb + _web,
      Env: onionweb_env,
      Expose: "80",
      HostConfig: { "NetworkMode": "deploy_isolated_tor_net" },
      autorestart: "ON_FAILURE"
    });
    
    await onionweb_container.start();
    console.log(snb, 'Onionweb container started, id=' + onionweb_container.id);

    console.log(snb, 'Starting onionssh with NET_ADMIN capability');
    const onionssh_container = await docker.createContainer({
        Image: 'onion_ssh',
        name: snb + _ssh,
        Env: onionssh_env,
        Expose: "22",
        HostConfig: { 
          "NetworkMode": personal_tor_network_name, 
          "Privileged": true, 
          "CapAdd": ["NET_ADMIN"] 
        },
        autorestart: "ALWAYS"
      });
    await onionssh_container.start(); // TODO: start by API call, as previous quests completed?
    console.log(snb, 'Onionssh container started, id=' + onionssh_container.id);
    
  /*  let cmd = ["sh", "-c", "printenv BOSS_KEY > /root/key.asc"];
    let docker_stream = await get_stream_from_command_in_container(onionssh_container, cmd);
    let cmd = ["sh", "-c", "printenv BOSS_KEY > /root/key.asc"];
    let docker_stream = await get_stream_from_command_in_container(onionssh_container, cmd);*/

    //let output = await read_chunk_from_stream(onionssh_container, docker_stream, "utf8");
    //console.log(snb, "Writing enc key, output: " + output);

    
    
    console.log(snb, 'Starting mitmquest container');
    const mitmquest_container = await docker.createContainer({
      Image: 'mitm_quest',
      name: snb + _boss,
      Env: mitmquest_env,
      HostConfig: { "NetworkMode": personal_tor_network_name },
      autorestart: "ALWAYS"
    }); 
    await mitmquest_container.start(); // TODO: start on SSH quest completion
    console.log(snb, 'Mitmquest container started, id=' + mitmquest_container.id);

    const response_json = { 
      "id": onionweb_container.id + "|" + onionssh_container.id + "|" + mitmquest_container.id 
    };
    console.log(snb, JSON.stringify(response_json));  
    res.status(200).json(response_json).send();
  } catch (err) {
    console.log(snb, err);
    res.status(503).json({"error":err.message}).send();
  }
});


router2.get('/:id', async (req, res) => {
  let snb = "undefined_snb";
  try { 
    console.log('v2: requested status of containers: ', req.params.id);
    const ids = req.params.id.split('|');

    const onionweb_container = docker.getContainer(ids[0]);
    const onionssh_container = docker.getContainer(ids[1]);
    const mitmquest_container = docker.getContainer(ids[2]);

    const onionweb_info = await onionweb_container.inspect();
    // Substring to remove initial slash and ending _web
    snb = onionweb_info.Name.substring(1, onionweb_info.Name.length - 4);
    console.log(snb, 'Corresponding service name: ', snb);
    
    const onionweb_name = snb + _web;
    const onionssh_name = snb + _ssh;
    const mitmquest_name = snb + _boss;
    
    containers = await docker.listContainers({ all: false });
    const onionspawn_container_info = findOnionspawnContainer(containers);
    const onionspawn_container = docker.getContainer(onionspawn_container_info.Id);
    
    // FIXME: validate! Path traversal below?
    const onionweb_path = '/var/lib/tor/hidden_services/' + onionweb_name + '/hostname';
    const onionssh_path = '/var/lib/tor/hidden_services/' + onionssh_name + '/hostname';
    
    const onionweb_hostname = await read_onion_hostname(onionspawn_container, onionweb_path);
    console.log(snb, 'Onionweb hostname: ' + onionweb_hostname);
    const onionssh_hostname = await read_onion_hostname(onionspawn_container, onionssh_path);
    console.log(snb, 'Onionssh hostname: ' + onionssh_hostname);

    // Quest_1
    let onionweb_accessed = false;
    let docker_stream = await get_file_size_in_container(onionweb_container, 
      "/var/log/nginx/access2.log");
    const size = await read_chunk_from_stream(onionweb_container, docker_stream, "utf8");
    if (size > 0) {
      onionweb_accessed = true;
    }
    console.log(snb, "Onionweb accessed: " + onionweb_accessed); 
    
    // TODO: add second part of the quest - log in
//    let onionweb_logged_in = false;
    let q1_progress = 0;
    if (onionweb_accessed)  { q1_progress = 1; }
//    if (onionweb_logged_in) { q1_progress = 2; }
    
    // Quest_2
    let onionssh_corrupted = false;
    let cmd = ['hostname', '-i'];
    let onionssh_ip = "";
    try {
      docker_stream = await get_stream_from_command_in_container(onionssh_container, cmd);
      onionssh_ip = await read_chunk_from_stream(onionssh_container, docker_stream, "utf8");
      if (onionssh_ip.length > 16) {
        console.log(snb, "Invalid onionssh_ip length: " + onionssh_ip.length);
        onionssh_ip = "corrupted";
        onionssh_corrupted = true;
      } else {
        // remove trailing \n
        onionssh_ip = onionssh_ip.substring(onionssh_ip, onionssh_ip.length - 1);
      }
    } catch (err) {
      console.log(snb, err);
      onionssh_ip = "corrupted";
      onionssh_corrupted = true;
    }
    console.log(snb, "Onionssh ip: " + onionssh_ip);
    
    let onionssh_accessed = false;
    cmd = ['grep', '-c', 'Failed password for', '/var/log/auth.log'];
    let num_of_failed_attempts = 0;
    try {
      docker_stream = await get_stream_from_command_in_container(onionssh_container, cmd);
      num_of_failed_attempts = parseInt(await read_chunk_from_stream(onionssh_container,
        docker_stream, "utf8"), 10);
      onionssh_accessed = (num_of_failed_attempts > 0);
    } catch (err) {
      console.log(snb, err);
      onionssh_corrupted = true;
      onionssh_accessed = true;
    }
    console.log(snb, `Onionssh accessed: ${onionssh_accessed}, attempts: ${num_of_failed_attempts}`);
    
    let onionssh_bruted = false;
    let num_of_successful_attempts = 0;
    cmd = ['grep', '-c', 'Accepted password for', '/var/log/auth.log'];
    try {
      docker_stream = await get_stream_from_command_in_container(onionssh_container, cmd);
      num_of_successful_attempts = parseInt(await read_chunk_from_stream(onionssh_container,
        docker_stream, "utf8"), 10);
      onionssh_bruted = (num_of_successful_attempts > 0);
    } catch (err) {
      console.log(snb, err);
      onionssh_corrupted = true;
      onionssh_bruted = true;
    }
    console.log(snb, `Onionssh bruted: ${onionssh_bruted}, success attempts: ${num_of_successful_attempts}`);
    
    // TODO: detect these conditions:
    let onionssh_exploit_tried = false;
    let onionssh_exploited = false;
    let onionssh_priv_escalated = false;
    let onionssh_key_found = false; // check access time of the key file?
    
    let q2_progress = 0;
    if (onionssh_accessed)       { q2_progress = 1; }
    if (onionssh_bruted)         { q2_progress = 2; }
//    if (onionssh_exploit_tried)  { q2_progress = 3; }
//    if (onionssh_exploited)      { q2_progress = 4; }
//    if (onionssh_priv_escalated) { q2_progress = 5; }
//    if (onionssh_key_found)      { q2_progress = 6; }
    
    cmd = ['hostname', '-i'];
    let mitmquest_ip = "";
    docker_stream = await get_stream_from_command_in_container(mitmquest_container, cmd);
    mitmquest_ip = await read_chunk_from_stream(mitmquest_container, docker_stream, "utf8");
    mitmquest_ip = mitmquest_ip.substring(0, mitmquest_ip.length - 1); // remove trailing \n
    console.log(snb, "Mitmquest ip: " + mitmquest_ip);
    
    cmd = ['printenv', 'BOSS_PASSWORD'];
    let mitmquest_password = "";
    docker_stream = await get_stream_from_command_in_container(mitmquest_container, cmd);
    mitmquest_password = await read_chunk_from_stream(mitmquest_container, docker_stream, "utf8");
    mitmquest_password = mitmquest_password.substring(0, mitmquest_password.length - 1); // remove trailing \n
    console.log(snb, "Mitmquest password: " + mitmquest_password);
    
    // TODO: detect these conditions:
    let onionssh_nmap_run = false; // check access time? (need to mount with atime support...)
    let onionssh_mitmtool_run = false; // check access time (bettercap or dsniff)
   
    // check DNS error because of mitm
    cmd = ['grep', '-c', 'getaddrinfo EAI_AGAIN', '/app/log.txt'];
    docker_stream = await get_stream_from_command_in_container(mitmquest_container, cmd);
    const dns_errors = parseInt(await read_chunk_from_stream(mitmquest_container,
      docker_stream, "utf8"), 10);
    const mitmquest_mitm_dns = (dns_errors > 0);
    console.log(snb, `Mitmquest dns: ${mitmquest_mitm_dns}, errors: ${dns_errors}`);

    // check subj pinning
    cmd = ['grep', '-c', 'subjFingerprint does not match', '/app/log.txt'];
    docker_stream = await get_stream_from_command_in_container(mitmquest_container, cmd);
    const subj_pin_errors = parseInt(await read_chunk_from_stream(mitmquest_container,
      docker_stream, "utf8"), 10);
    const mitmquest_mitm_pinned = (subj_pin_errors > 0);
    console.log(snb, `Mitmquest subj pinned: ${mitmquest_mitm_pinned}, errors: ${subj_pin_errors}`);
    
    // check successfull mitm
    cmd = ['grep', '-c', 'MITM detected', '/app/log.txt'];
    docker_stream = await get_stream_from_command_in_container(mitmquest_container, cmd);
    const mitm_detects = parseInt(await read_chunk_from_stream(mitmquest_container,
      docker_stream, "utf8"), 10);
    const mitmquest_mitm_done = (mitm_detects > 0);
    console.log(snb, `Mitmquest done: ${mitmquest_mitm_done}, detects: ${mitm_detects}`);

    let q3_progress = 0;
//    if (onionssh_nmap_run)     { q3_progress = 1; }
//    if (onionssh_mitmtool_run) { q3_progress = 2; }
    if (mitmquest_mitm_dns)    { q3_progress = 1; }
    if (mitmquest_mitm_pinned) { q3_progress = 2; }
    if (mitmquest_mitm_done)   { q3_progress = 3; }
// quest mapping: 1,2,3 -> 3,4,5  
  
    const response_json = {"quests": [
      { "name":"quest_4", "current_progress":q1_progress, "progress_max":1,
        "hostname":onionweb_hostname },
      { "name":"quest_5", "current_progress":q2_progress, "progress_max":3,
        "hostname": onionssh_hostname, "ip": onionssh_ip, "corrupted": onionssh_corrupted },
      { "name":"quest_6", "current_progress":q3_progress, "progress_max":3,
        "ip": mitmquest_ip, "password": mitmquest_password } 
    ]};
    
    console.log(snb, JSON.stringify(response_json));  
    res.status(200).json(response_json).send();

  } catch (err) {
    console.log(snb, err);
    res.status(503).json({"error":err.message}).send();
  }
});

async function get_file_size_in_container(container, path) {
  let cmd = ['stat', '-c %s', path]; // BusyBox; --printf="%s" for normal stat...
  return get_stream_from_command_in_container(container, cmd);
}

async function get_stream_from_file_in_container(container, path) {
  let cmd = ["cat", path];
  return get_stream_from_command_in_container(container, cmd);
}

async function get_stream_from_command_in_container(container, cmd) {
  var options = {
    Cmd: cmd,
    AttachStdout: true,
    AttachStderr: true
  };
  const exec = await container.exec(options);
  
  return new Promise((resolve, reject) => {
    exec.start((err, stream) => {
      if (err) reject(err);
      else resolve(stream);
    });
  });
}

async function read_chunk_from_stream(container, docker_stream, encoding) {
  let docker_stdout = new stream.PassThrough();
  let docker_stderr = new stream.PassThrough();
  docker_stdout.setEncoding(encoding);
  docker_stderr.setEncoding(encoding);

  container.modem.demuxStream(docker_stream, docker_stdout, docker_stderr);
  return new Promise((resolve, reject) => {
    docker_stderr.on("data", (chunk) => { return reject(new Error(chunk)); });
    docker_stderr.on("error", (error) => { return reject(error); });
    
    docker_stdout.on("data", (chunk) => { return resolve(chunk); });
    docker_stdout.on("error", (error) => { return reject(error); });
    docker_stdout.on("end", () => { return resolve(""); });
  });
}

// Encrypts infile and saves to outfile using password (via gpg)
async function encrypt_key(infile, outfile, password) {
return new Promise((resolve, reject) => {
    gpg.call(password, 
      ["--passphrase-fd", "0", "--batch", "-ca", "-o", outfile, infile], 
      (err, data) => {
        if (err) reject(err);
        else resolve(data);
      }
    );
  });
}

/*
async function ensure_no_data_from_stream(stream) {
  return new Promise((resolve, reject) => {
    stream.on("data", (chunk) => { return reject(new Error(chunk)); });
    stream.on("error", (error) => { return reject(error); });
    stream.on("end", () => { return resolve() });
  });
}*/

/*
function demuxStream(stream, stdout, stderr) {
  var header = null;

  stream.on('readable', function() {
    header = header || stream.read(8);
    while (header !== null) {
      console.log("Header: %s", util.inspect(header));
      var type = header.readUInt8(0);
      var payload = stream.read(header.readUInt32BE(4));
      if (payload === null) break;
      if (type == 2) {
        stderr.write(payload);
      } else {
        stdout.write(payload);
      }
      header = stream.read(8);
    }
  });
};*/

async function read_onion_hostname(onionspawn_container, path) {
  const docker_stream = await get_stream_from_file_in_container(onionspawn_container, path);
  
  const chunk = await read_chunk_from_stream(onionspawn_container, docker_stream, "ascii");
  const onion_name_len = 22; // 16 + 6 for .onion 
  if (chunk.length <= onion_name_len) { // Normally we get 23; if less, than there is no onion hostname yet
    return new Promise((resolve, reject) => { resolve(""); }); // empty hostname
  } else if (chunk.length > onion_name_len + 1) { // if more, than the file is seriously corrupted
    return new Promise((resolve, reject) => { reject(new Error("Invalid hostname file")); });
  }
  // skipping final \n
  const hname = chunk.substring(0, chunk.length - 1);
  return new Promise((resolve, reject) => { resolve(hname); });
}

app.use('/v2/onions', router2);

app.get('/', (req, res) => res.json({ message:
  'DO NOT HACK pls -- internal quest infrastructure\n\
   POST /v1/onions ContentType: application/json {"service_name": "unixname"}\n\
.....Creates & start new onion\n\
.....200, {"id":"dockerid"} on success.\n\
.....503, {"error":"Error msg"} on error.\n\n\
   GET /v1/onions/:id (no body)\n\
.....Query status of the onion.\n\
.....200, {"hostname":""} if onion hostname isn\'t created yet.\n\
.....200, {"hostname":"deadbeefdeadbeef.onion"} if onion exists.\n\
.....503, {"error":"Error msg"} on error.'}));

app.listen(port, () => console.log('Supervisor listening on port ' + port));

// POST /v1/onions - create & start new onion
// GET /v1/onions/:id - return info about onion with id

// Potentail API (needed?):
// GET /v1/onions -- return all running onions
// DELETE /v1/onions/:id - stop & delete onion
// PUT /v1/onions/:id to start,stop,restart onion?

// curl -X GET -H "Content-Type: application/json" "http://localhost:3000/v2/onions/<id>"
//  curl -X POST -H "Content-Type: application/json" http://localhost:3000/v1/onions -d "{\"service_name\":\"abc12\"}"



