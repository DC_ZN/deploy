#!/usr/bin/env bash

# Global
APP="/app"

create_database() {
  node ${APP}/scripts/create_database.js
}

run_service() {
  pushd ${APP} > /dev/null
  node server.js
}

# This may be needed in dev env, when /app is bind-mounted (or is docker volume)
# In this case npm install in Dockerfile is not enough
npm_install() {
  cd ${APP}
  npm install
}

generate_settings() {
  # Determine docker's host address and apply it to the config file template
  docker_host=`netstat -nr | grep '^0\.0\.0\.0' | awk '{print $2}'`
  sed -E "s/docker_host_address/$docker_host/g" "${APP}/config/const_settings.js.template" > "${APP}/config/const_settings.js"

  database_host=`getent hosts database | awk '{print $1}'`
  sed -E "s/database_address/$database_host/g" "${APP}/config/database.js.template" > "${APP}/config/database.js"
}

###############################################################################

main() {
  echo "Installing modules ..."
  npm_install
  echo "Generating settings from template ..."
  generate_settings
  echo "Creating database if it does not exist ..."
  create_database
  echo "Starting service ..."
  run_service
}

###############################################################################

main "$@"
