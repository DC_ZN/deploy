#!/bin/bash

set -e

host="$1"
shift
cmd="$@"

until nc -w 1 database 3306 > /dev/null; do
  >&2 echo "Sleeping"
  sleep 5
done

>&2 echo "MySQL is up"
exec $cmd
