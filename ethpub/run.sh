#!/usr/bin/env bash

datadir="/datadir"
networkid="13020"

ip=$(lookup minernode)
enode="e0e362523a35eea83c499a6a2b38ddad0013ea6e0210795298b836fbffcd64532ab1e3eb815a8d8d01cc9d2cbd2c1f6e8730f05bc66cfa3b8cd55345d949ce58"
node_addr="enode://$enode@$ip:30303"
echo "[ \"$node_addr\" ]" > $datadir/static-nodes.json

geth --nat none --nodekey /pub_node.key --bootnodes $node_addr --nodiscover --networkid $networkid --datadir=$datadir --ipcdisable --rpc --rpcaddr 0.0.0.0 --rpcapi eth,web3 --rpccorsdomain "*"
