#!/bin/bash

set -e

host="$1"
shift
cmd="$@"

until nc -w 1 minernode 30303 > /dev/null; do
  >&2 echo "Sleeping"
  sleep 5
done

>&2 echo "Miner node is up"
exec $cmd
