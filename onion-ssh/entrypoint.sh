#!/bin/bash
if [ -x /root/entrypoint-once.sh ]; then
  /root/entrypoint-once.sh
fi

unset GOODKEY

service syslog-ng start
service ssh start 
tail -f /etc/hosts
