#!/usr/bin/env bash

datadir="/datadir"
networkid="13020"

fund_holder_acct="0xe278e27e4a9e2dc6d9a52dfcb10e1c538c2087cd"
fund_holder_pass="funding_account_#456"

ip=$(lookup minernode)
enode="e0e362523a35eea83c499a6a2b38ddad0013ea6e0210795298b836fbffcd64532ab1e3eb815a8d8d01cc9d2cbd2c1f6e8730f05bc66cfa3b8cd55345d949ce58"
node_addr="enode://$enode@$ip:30303"
echo "[ \"$node_addr\" ]" > $datadir/static-nodes.json

geth --nodekey /rpc_node.key --bootnodes $node_addr --nodiscover --networkid $networkid --datadir=$datadir --ipcdisable --rpc --unlock $fund_holder_acct --password <(echo $fund_holder_pass) --rpcaddr 127.0.0.1 --rpcapi eth,net,web3,personal &

node index.js

