#!/usr/bin/env bash

################################################################################

NORM=0
BOLD=1
UNLN=4
RED=31
GREEN=32
BROWN=33
BLUE=34
MAG=35
CYAN=36
GREY=37

CL_NORM="\e[${NORM}m"
CL_BOLD="\e[${BOLD}m"
CL_UNLN="\e[${UNLN}m"
CL_RED="\e[${RED}m"
CL_GREEN="\e[${GREEN}m"
CL_BROWN="\e[${BROWN}m"
CL_BLUE="\e[${BLUE}m"
CL_MAG="\e[${MAG}m"
CL_CYAN="\e[${CYAN}m"
CL_GREY="\e[${GREY}m"
CL_BL_RED="\e[${RED};1m"
CL_BL_GREEN="\e[${GREEN};1m"
CL_BL_BROWN="\e[${BROWN};1m"
CL_BL_BLUE="\e[${BLUE};1m"
CL_BL_MAG="\e[${MAG};1m"
CL_BL_CYAN="\e[${CYAN};1m"
CL_BL_GREY="\e[${GREY};1m"
CL_UL_RED="\e[${RED};4m"
CL_UL_GREEN="\e[${GREEN};4m"
CL_UL_BROWN="\e[${BROWN};4m"
CL_UL_BLUE="\e[${BLUE};4m"
CL_UL_MAG="\e[${MAG};4m"
CL_UL_CYAN="\e[${CYAN};4m"
CL_UL_GREY="\e[${GREY};4m"
CL_BG_RED="\e[${RED};7m"
CL_BG_GREEN="\e[${GREEN};7m"
CL_BG_BROWN="\e[${BROWN};7m"
CL_BG_BLUE="\e[${BLUE};7m"
CL_BG_MAG="\e[${MAG};7m"
CL_BG_CYAN="\e[${CYAN};7m"
CL_BG_GREY="\e[${GREY};7m"

###############################################################################

show() {
    if [[ -n "$2" && -z "$no_colors" ]] ; then
        echo -e "\e[${2}m${1}\e[0m"
    else
        echo -e "$1"
    fi
}

###############################################################################

usage() {
  show "Usage: $0 ${CL_CYAN}<ACTION>${CL_NORM} <username>" ${BOLD}
  show "ACTIONS:" ${BOLD}
  show "    list       - list the quest containers of the specified user" ${BOLD}
  show "    listall    - list the quest containers of all users" ${BOLD}
  show "    start      - start the quest containers of the specified user" ${BOLD}
  show "    startall   - start the quest containers of all users" ${BOLD}
  show "    stop       - stop the quest containers of the specified user" ${BOLD}
  show "    stopall    - stop the quest containers of all users" ${BOLD}
  show "    restart    - restart the quest containers of the specified user" ${BOLD}
  show "    restartall - restart the quest containers of all users" ${BOLD}
  show "    delete     - delete the quest containers of the specified user" ${BOLD}
  show "    deleteall  - delete the quest containers of all" ${BOLD}
  exit 1
}

delete_networks() {
  local username=${1:-"ALL"}

  if [ "${username}" == "ALL" ];then
    for n in $(docker network ls | grep "_pnet " | awk '{print $1}'); do
      docker network disconnect -f ${n} deploy_onionspawn_1
      docker network rm ${n}
    done
  else
    for n in $(docker network ls | grep "${username}_pnet " | awk '{print $1}'); do
      docker network disconnect -f ${n} deploy_onionspawn_1
      docker network rm ${n}
    done
  fi
}

list_containers() {
  local username=${1:-"ALL"}

  if [ "${username}" == "ALL" ];then
    docker ps --format '{{.Names}}\t{{.Image}}\t{{.Status}}' | grep "mitm_quest\|onion_ssh\|onion_web"
  else
    docker ps --format '{{.Names}}\t{{.Image}}\t{{.Status}}' | grep "^${username}"
  fi
}

doaction_containers() {
  local action=${1}
  local username=${2:-"ALL"}

  if [ "${username}" == "ALL" ];then
    for c in $(docker ps -a | grep "_web$\|_ssh$\|_boss$" | awk '{print $1}'); do
      docker ${action} ${c}
      if [ "${action}" == "delete" ];then
        docker rm ${c}
      fi
    done
  else
    for c in $(docker ps -a | grep " ${username}" | awk '{print $1}'); do
      docker ${action} ${c}
      if [ "${action}" == "delete" ];then
        docker rm ${c}
      fi
    done
  fi
}

delete_containers(){
  local username=${1:-"ALL"}

  if [ "${username}" == "ALL" ];then
    for c in $(docker ps -a | grep "_web$\|_ssh$\|_boss$" | awk '{print $1}'); do
      docker stop ${c}
      docker rm ${c}
    done
    delete_networks
  else
    for c in $(docker ps -a | grep " ${username}" | awk '{print $1}'); do
      docker stop ${c}
      docker rm ${c}
    done
    delete_networks "${username}"
  fi

}

###############################################################################

main() {

    # if [ $# -ne 2 ];then
    #   usage
    # fi

    USER="${2}"
    case "${1}" in
      list)
        show "Quest containers for user: ${CL_CYAN}${USER}${CL_NORM}" ${BOLD}
        show ""
        list_containers "${USER}"
        ;;
      listall)
        show "Quest containers for ${CL_CYAN}all${CL_NORM} users" ${BOLD}
        show ""
        list_containers
        ;;
      start)
        show "Starting quest containers for user: ${CL_CYAN}${USER}${CL_NORM}" ${BOLD}
        show ""
        doaction_containers "start" "${USER}"
        ;;
      startall)
        show "Starting all quest containers" ${BOLD}
        show ""
        doaction_containers "start"
        ;;
      stop)
        show "Stopping quest containers for user: ${CL_CYAN}${USER}${CL_NORM}" ${BOLD}
        show ""
        doaction_containers "stop" "${USER}"
        ;;
      stopall)
        show "Stopping all quest containers" ${BOLD}
        show ""
        doaction_containers "stop"
        ;;
      restart)
        show "Restarting quest containers for user: ${CL_CYAN}${USER}${CL_NORM}" ${BOLD}
        show ""
        doaction_containers "restart" "${USER}"
        ;;
      restartall)
        show "Restarting all quest containers" ${BOLD}
        show ""
        doaction_containers "restart"
        ;;
      delete)
        show "Deleting quest containers for user: ${CL_CYAN}${USER}${CL_NORM}" ${BOLD}
        show ""
        delete_containers "${USER}"
        ;;
      deleteall)
        show "Deleting all quest containers" ${BOLD}
        show ""
        delete_containers
        ;;
      *)
        usage
        ;;
    esac

    show ""
    show "Done" $GREEN
    show ""
}

###############################################################################

main "$@"
