#!/usr/bin/env bash

# Author:  Extor
# License:  GPLv3

# DESCRIPTION
# Script for renew the LE certificates. It's using dns-cloudflare plugin
# for certbot.
# REQUIREMENTS
# 1. File with credentials, placed to the /root/config/certbot/do.ini by default
# 2. Installed certbot
# USAGE EXAMPLE
# le_cert_update.sh create ~/.config/certbot/do.ini

###############################################################################

NORM=0
BOLD=1
UNLN=4
RED=31
GREEN=32
BROWN=33
BLUE=34
MAG=35
CYAN=36
GREY=37

###############################################################################

LOGFILE=${LOGFILE:-/var/log/le_certs_update.log}

show() {
    if [[ -n "$2" ]] ; then
        echo -e "\e[${2}m${1}\e[0m"
    else
        echo -e "$1"
    fi
}

usage() {
  show "$0 <create|update> [/certbot/digitalocean/cred/file]" ${BROWN}
  show ""
  exit 1
}

checktools(){
  local local_tools="certbot"
  for tool in ${local_tools}; do
    binpath=$(/usr/bin/whereis ${tool} | awk '{print $2}')
    if [ "${binpath}" == "" ];then
      show "ERROR: ${tool} not found. " $RED
      log ${LOGFILE} "${tool} not found."
      exit 1
    fi
  done
}

checkcmdresult() {
  local err_msg=${1:-'Somethig wrong'}
  if [ $? -ne 0 ]; then
    show "ERROR: Something wrong ..." $RED
    log "${LOGFILE}" "ERROR: ${err_msg}"
    exit 1
  fi
}

log() {
  local message="${2}"
  local logfile="${1}"

  local date=$(date '+%d-%m-%Y %H:%M:%S')
  echo "[${date}] ${message}" >> ${logfile}

}

###############################################################################

cert_create () {
    local cred_file="${1}"

    # ToDo: remove hardcoded domains
    certbot certonly -n --dns-digitalocean \
      --dns-digitalocean-credentials ${cred_file} \
      -d yo-corp.ru \
      -d www.yo-corp.ru \
      -d wallet.yo-corp.ru \
      -d comments.yo-corp.ru \
      --agree-tos \
      -q
    checkcmdresult "Can not create certificates"
    log "${LOGFILE}" "Certificates created"
}

cert_renew () {
    local cred_file="${1}"

    # ToDo: remove hardcoded domains
    certbot renew --dns-digitalocean \
      --dns-digitalocean-credentials ${cred_file} \
      -q
    checkcmdresult "Can not update certificates"
    log "${LOGFILE}" "Certificates successfully updated or no updates required"
}

main () {

    ACTION=${1}
    CRED_FILE=${2:-~/.config/certbot/do.ini}

    if [[ $# -lt 1 || $# -gt 2 ]];then
      usage
    fi

    case ${ACTION} in
      create)
        show "Getting new certificates ..." ${BOLD}
        log ${LOGFILE} "Getting new certificates ..."
        cert_create "${CRED_FILE}"
      ;;
      update)
        show "Updating certificates ..." $BOLD
        log ${LOGFILE} "Updating certificates ..."
        cert_renew "${CRED_FILE}"
      ;;
      *)
        usage
      ;;
    esac

    show "Done" ${GREEN}
    show ""
    exit 0
}

###############################################################################

main "$@"
